import React from 'react'
import Draggable from 'react-draggable';
import './MemeGenerator.css'
const TextComponent = ({TextValue}) => {
    return (
        <Draggable>
            <div className='box'>
                <h2>{TextValue}</h2>
            </div>
        </Draggable>
    )
}
export default TextComponent