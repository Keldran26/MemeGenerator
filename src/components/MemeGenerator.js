import React from 'react'
import './MemeGenerator.css'
import List from './List'
import Image from './Image'
import TextComponentList from './TextComponentList'

class MemeGenerator extends React.Component {
    constructor() {
        super()
        this.state = {
            TextValue: '',
            GenerateMeme:'',
            MemeImgUrl: '',
            allMemeImgs: [],
            textArray:[]
            
        }
    }
    componentDidMount = (event) => {
        fetch("https://api.imgflip.com/get_memes")
          .then(response => response.json())
          .then(response => {
        const { memes } = response.data
        this.setState({ allMemeImgs: memes })
      })
    }

    handleSubmit = (event) => {
        event.preventDefault()
    }

    handleChange = (event) => {
        const { name, value } = event.target
        this.setState({[name]:value})
    }

    AddText =() =>{
        console.log("Add text")
        var newArray = this.state.textArray;
        newArray.push(this.state.TextValue)

        this.setState({textArray:newArray,TextValue:''})
    }
    
    AddImage = () => {
        for(var i = 0; i<this.state.allMemeImgs.length;i++){
            if(this.state.allMemeImgs[i].name===this.state.GenerateMeme)
            {
                this.setState({MemeImgUrl: this.state.allMemeImgs[i].url})
            }
        }
    }
        
    render() {
        return(
            <div className="MemeGenerator">
                <div className="UI">
                    <h1 style={{textAlign:'center'}}>Generator Memow</h1>
                    <div className="FormDiv">
                        <form className="meme-form" onSubmit={this.handleSubmit}>
                            <input type="text" name="GenerateMeme" placeholder="Image name" value={this.state.GenerateMeme} onChange={this.handleChange}/>
                            <input type="text" name="TextValue" placeholder="Text Value" value={this.state.TextValue} onChange={this.handleChange}/>
                            <button onMouseDown={this.AddImage}>AddImage</button>
                            <button onMouseDown={this.AddText}>AddText</button>
                        </form>
                    </div>
                </div>
                <div className="container">
                    <div className='child' id="capture">
                            <TextComponentList Texts={this.state.textArray}/>
                            <Image image={this.state.MemeImgUrl}/>
                    </div>
                    <div className="child">
                        <ol>
                            <List memes={this.state.allMemeImgs}/>
                        </ol>
                        
                    </div>
                </div>
                
            </div>
        )
    }
}

  
  
export default MemeGenerator