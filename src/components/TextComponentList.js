import TextComponent from "./TextComponent"

const TextComponentList = ({Texts}) => {
    const renderedList = Texts.map((text) => {
        return <TextComponent TextValue = {text}/>;
    })
    return <div>{renderedList}</div>
}
export default TextComponentList 