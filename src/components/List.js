import ItemList from "./ItemList"

const List = ({memes}) => {
    const renderedList = memes.map((meme) => {
        return <ItemList meme = {meme}/>;
    })
    return <div>{renderedList}</div>
}
export default List